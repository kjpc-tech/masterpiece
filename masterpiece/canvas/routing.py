from django.urls import path

from . import consumers


websocket_urlpatterns = [
    path('ws/artist/', consumers.ArtistConsumer),
]
