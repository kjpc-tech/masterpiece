from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


class Canvas(models.Model):
    name = models.CharField(max_length=100, unique=True)
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Canvases"

    def __str__(self):
        return self.name


class Pixel(models.Model):
    canvas = models.ForeignKey(Canvas, on_delete=models.CASCADE)
    x = models.PositiveIntegerField()
    y = models.PositiveIntegerField()
    color = models.CharField(max_length=7, validators=[
                RegexValidator(r'#\d{6}', "Color must be in '#RRGGBB' format.")])
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return "{}, {} on {}".format(self.x, self.y, str(self.canvas))

    def clean(self):
        validation_errors = {}

        if self.x >= self.canvas.width:
            validation_errors['x'] = f"X '{self.x}' is not on canvas with max x of '{self.canvas.width - 1}'."
        if self.y >= self.canvas.height:
            validation_errors['y'] = f"Y '{self.y}' is not on canvas with max y of '{self.canvas.height - 1}'."

        if len(validation_errors.keys()) > 0:
            raise ValidationError(validation_errors)
