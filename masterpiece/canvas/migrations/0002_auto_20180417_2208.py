# Generated by Django 2.0.4 on 2018-04-17 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('canvas', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='canvas',
            options={'verbose_name_plural': 'Canvases'},
        ),
        migrations.AlterModelOptions(
            name='pixel',
            options={'ordering': ['-created']},
        ),
        migrations.AlterField(
            model_name='canvas',
            name='name',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
