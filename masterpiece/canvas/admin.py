from django.contrib import admin


from .models import Canvas, Pixel


@admin.register(Canvas)
class CanvasAdmin(admin.ModelAdmin):
    list_display = ['name', 'width', 'height', 'created']
    date_hierarchy = 'created'


@admin.register(Pixel)
class PixelAdmin(admin.ModelAdmin):
    list_display = ['canvas', 'x', 'y', 'color', 'active', 'created']
    list_filter = ['canvas', 'active']
    date_hierarchy = 'created'
