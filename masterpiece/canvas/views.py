from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_GET

from .models import Canvas, Pixel


class CanvasView(TemplateView):
    template_name = 'canvas/canvas.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        canvas, _ = Canvas.objects.get_or_create(
            name='Default Canvas',
            defaults={
                'width': 1920,
                'height': 1080,
            }
        )

        context['canvas'] = canvas

        return context


@require_GET
def canvas_pixels(request):
    canvas = get_object_or_404(Canvas, pk=request.GET.get('canvas'))

    return JsonResponse({
        'pixels': list(Pixel.objects.filter(
            canvas=canvas,
            active=True,
        ).values('x', 'y', 'color')),
    })
