from django.conf import settings


def canvas(request):
    return {
        'google_analytics': getattr(settings, 'GOOGLE_ANALYTICS', None)
    }
