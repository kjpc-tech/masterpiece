const vue = new Vue({
  el: "#holder",
  delimiters: ['((', '))'],

  data: {
    canvas: null,
    ctx: null,
    pixels: [],
    pixels_history: '',
    canvas_id: '',
    scheme: '',
    color: '',
    socket: null,
  },

  mounted: function() {
    var self = this;

    this.canvas = this.$refs.canvas;
    this.ctx = this.canvas.getContext('2d');

    this.pixels_history = this.$refs.pixels_history.getAttribute('data-default');
    this.canvas_id = this.$refs.canvas_id.getAttribute('data-default');
    this.color = this.$refs.color.getAttribute('data-default');

    this.draw_background();

    this.load_pixels();

    var scheme = this.$refs.scheme.getAttribute('data-default');
    var socket_scheme = '';
    if (scheme.toLowerCase() == 'https') {
      socket_scheme = 'wss';
    } else {
      socket_scheme = 'ws';
    }
    this.socket = new WebSocket(socket_scheme + '://' + window.location.host + '/ws/artist/');

    this.socket.onopen = function(e) {
      console.log('Socket is open :)');
    };

    this.socket.onmessage = function(e) {
      var data = JSON.parse(e.data);
      var message = data.message;
      self.draw_pixel(message.x, message.y, message.color);
    };

    this.socket.onerror = function(e) {
      console.error('Socket had an unexpected error :(');
    };

    this.socket.onclose = function(e) {
      console.error('Socket closed unexpectedly :(');
    };
  },

  methods: {
    load_pixels: function() {
      this.$http.get(this.pixels_history, {
        params: {
          canvas: this.canvas_id,
        },
      }).then(response => {
        this.pixels = response.data.pixels;
        this.draw_initial();
      }, response => {
        // error
        console.error("Loading pixels failed.", response);
      });
    },

    draw_background: function() {
      this.ctx.fillStyle = '#000000';
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    },

    draw_initial: function() {
      for (var p = 0; p < this.pixels.length; p++) {
        this.draw_pixel(this.pixels[p].x, this.pixels[p].y, this.pixels[p].color);
      }
    },

    draw_pixel: function(x, y, color) {
      this.ctx.fillStyle = color;
      this.ctx.fillRect(x, y, 1, 1);
    },

    color_pixel: function(event) {
      var x = event.offsetX * this.canvas.width / this.canvas.offsetWidth;
      var y = event.offsetY * this.canvas.height / this.canvas.offsetHeight;

      // send pixel to server
      this.socket.send(JSON.stringify({
        'message': {
          'canvas': this.canvas_id,
          'x': x,
          'y': y,
          'color': this.color,
        },
      }));

      // draw pixel locally
      this.draw_pixel(x, y, this.color);
    },
  },
});
