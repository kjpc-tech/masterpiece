from django.apps import AppConfig


class CanvasConfig(AppConfig):
    name = 'canvas'

    verbose_name = 'Canvas App'

    def ready(self):
        import canvas.signals
