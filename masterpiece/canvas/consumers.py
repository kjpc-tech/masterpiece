import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from .models import Canvas, Pixel

GROUP_NAME = "artists_1"


class ArtistConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add(
            GROUP_NAME,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            GROUP_NAME,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # create new pixel
        await database_sync_to_async(Pixel.objects.create)(
            canvas=Canvas.objects.get(pk=message['canvas']),
            x=message['x'],
            y=message['y'],
            color=message['color'],
        )

        # forward message to group
        await self.channel_layer.group_send(
            GROUP_NAME,
            {
                'type': 'canvas_update',
                'message': message,
            }
        )

    async def canvas_update(self, event):
        message = event['message']

        # forward new pixel information to everybody
        await self.send(text_data=json.dumps({
            'message': message,
        }))
