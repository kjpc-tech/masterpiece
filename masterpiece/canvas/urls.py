from django.urls import path

from . import views


app_name = 'canvas'

urlpatterns = [
    path('', views.CanvasView.as_view(), name='canvas'),
    path('pixels/', views.canvas_pixels, name='pixels'),
]
