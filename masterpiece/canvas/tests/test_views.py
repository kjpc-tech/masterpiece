import json

from django.test import TestCase
from django.urls import reverse

from canvas.models import Canvas, Pixel


class CanvasViewTests(TestCase):
    def setUp(self):
        self.canvas = Canvas.objects.create(
            name="Default Canvas",
            width=100,
            height=100,
        )

        for i in range(100):
            Pixel.objects.create(
                canvas=self.canvas,
                x=i,
                y=i,
                color="#123456",
            )

    def test_index_url_routes_here(self):
        response = self.client.get('/', secure=True)

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'canvas/canvas.html')

    def test_template_and_context(self):
        response = self.client.get(reverse('canvas:canvas'), secure=True)

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'canvas/canvas.html')

        self.assertIn('canvas', response.context.keys())

    def test_default_canvas_created_if_doesnt_exist(self):
        self.canvas.delete()

        self.assertFalse(Canvas.objects.filter(name="Default Canvas").exists())

        self.client.get(reverse('canvas:canvas'), secure=True)

        self.assertTrue(Canvas.objects.filter(name="Default Canvas").exists())


class CanvasPixelsViewTests(TestCase):
    def setUp(self):
        self.canvas = Canvas.objects.create(
            name="Default Canvas",
            width=100,
            height=100,
        )

        for i in range(100):
            Pixel.objects.create(
                canvas=self.canvas,
                x=i,
                y=i,
                color="#123456",
            )

    def test_correct_canvas_pixels_are_in_context(self):
        another_canvas = Canvas.objects.create(
            name="Another Canvas",
            width=100,
            height=100,
        )

        for i in range(100):
            Pixel.objects.create(
                canvas=another_canvas,
                x=99 - i,
                y=99 - i,
                color="#987654",
            )

        response = self.client.get(reverse('canvas:pixels'), {'canvas': another_canvas.pk}, secure=True)

        self.assertEqual(response.status_code, 200)

        pixels = json.loads(response.content)['pixels']
        self.assertEqual(len(pixels), 100)
        for pixel in pixels:
            self.assertEqual(pixel['color'], "#987654")

        response = self.client.get(reverse('canvas:pixels'), {'canvas': self.canvas.pk}, secure=True)

        self.assertEqual(response.status_code, 200)

        pixels = json.loads(response.content)['pixels']
        self.assertEqual(len(pixels), 100)
        for pixel in pixels:
            self.assertEqual(pixel['color'], "#123456")
