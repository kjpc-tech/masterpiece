from django.urls import reverse

from masterpiece.tests.tests import MySeleniumTestCase

from canvas.models import Pixel


class CanvasFlowTests(MySeleniumTestCase):
    def test_canvas_click_creates_pixel(self):
        self.selenium.get("{}{}".format(self.live_server_url, reverse('canvas:canvas')))

        self.assertEqual(Pixel.objects.all().count(), 0)

        actions = self.ActionChains(self.selenium)
        actions.move_to_element_with_offset(self.selenium.find_element_by_css_selector("canvas#canvas"), 20, 20)
        actions.click()
        actions.perform()

        self.assertEqual(Pixel.objects.all().count(), 1)
