from django.test import TestCase

from canvas.models import Canvas, Pixel


class CanvasModelTests(TestCase):
    def test_resize_canvas_deactivates_floating_pixels(self):
        # create canvas
        canvas = Canvas.objects.create(
            name="Test Canvas",
            width=100,
            height=100,
        )

        # create line of pixels filling image width
        for i in range(100):
            Pixel.objects.create(
                canvas=canvas,
                x=i,
                y=5,
                color="#123456",
            )

        # all pixels should be active
        self.assertEqual(Pixel.objects.filter(canvas=canvas, active=True).count(), 100)

        # resize canvas
        canvas.width = 50
        canvas.height = 50
        canvas.save()

        # now only half the pixels should be active
        self.assertEqual(Pixel.objects.filter(canvas=canvas, active=True).count(), 50)


class PixelModelTests(TestCase):
    def test_create_pixel_deactivates_hidden_pixels(self):
        # create canvas
        canvas = Canvas.objects.create(
            name="Test Canvas",
            width=100,
            height=100,
        )

        # create some pixels
        Pixel.objects.create(
            canvas=canvas,
            x=1,
            y=1,
            color="#123456",
        )
        Pixel.objects.create(
            canvas=canvas,
            x=2,
            y=2,
            color="#123456",
        )
        Pixel.objects.create(
            canvas=canvas,
            x=3,
            y=3,
            color="#123456",
        )

        # all pixels should be active
        self.assertEqual(Pixel.objects.filter(canvas=canvas, active=True).count(), 3)

        # create pixel over another pixel
        Pixel.objects.create(
            canvas=canvas,
            x=2,
            y=2,
            color="#654321",
        )

        # 3 out of 4 pixels should be active
        self.assertEqual(Pixel.objects.filter(canvas=canvas, active=True).count(), 3)

        # this pixel should not be active
        self.assertFalse(Pixel.objects.get(canvas=canvas, x=2, y=2, color="#123456").active)
