from django.dispatch import receiver
from django.db.models import Q
from django.db.models.signals import post_save

from .models import Canvas, Pixel


@receiver(post_save, sender=Canvas)
def deactivate_floating_pixels(sender, instance, created, **kwargs):
    if not created:
        # deactivate any pixels that are no longer on the canvas
        Pixel.objects.filter(
            Q(x__gte=instance.width) | Q(y__gte=instance.height),
            canvas=instance,
            active=True,
        ).update(active=False)


@receiver(post_save, sender=Pixel)
def deactivate_hidden_pixels(sender, instance, created, **kwargs):
    if created:
        # deactivate any pixels that this new pixel covers
        Pixel.objects.filter(
            canvas=instance.canvas,
            x=instance.x,
            y=instance.y,
            active=True,
        ).exclude(pk=instance.pk).update(active=False)
