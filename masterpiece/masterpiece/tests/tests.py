import unittest

from django.conf import settings

from channels.testing import ChannelsLiveServerTestCase

if settings.RUN_SELENIUM_TESTS:
    from selenium.webdriver.firefox.webdriver import WebDriver
    from selenium.webdriver.common.action_chains import ActionChains


@unittest.skipUnless(settings.RUN_SELENIUM_TESTS, "RUN_SELENIUM_TESTS is False")
class MySeleniumTestCase(ChannelsLiveServerTestCase):
    serve_static = True

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        cls.ActionChains = ActionChains

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
