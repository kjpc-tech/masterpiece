from split_settings.tools import optional, include

include(
    'settings/base.py',
    optional('settings/development.py'),
    optional('settings/production.py'),
)
