import os
import re
import urllib.request

from invoke import task, Collection

namespace = Collection()

current_dir = os.path.dirname(os.path.abspath(__file__))
venv = os.path.join(current_dir, "venv")
site_dir = os.path.join(current_dir, "masterpiece")
settings_dir = os.path.join(site_dir, "masterpiece", "settings")


@task
def check(ctx, warnings=False):
    """
    Run the manage.py check command.
    """
    warnings = " -Wall" if warnings else ""
    cmd = f'python{warnings} manage.py check'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task
def test(ctx, warnings=False):
    """
    Run the manage.py test command.
    """
    warnings = " -Wall" if warnings else ""
    cmd = f'python{warnings} manage.py test'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task
def makemigrations(ctx):
    """
    Run the manage.py makemigrations command.
    """
    cmd = 'python manage.py makemigrations'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task
def migrate(ctx):
    """
    Run the manage.py migrate command.
    """
    cmd = 'python manage.py migrate'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task
def createsuperuser(ctx):
    """
    Run the manage.py createsuperuser command.
    """
    cmd = 'python manage.py createsuperuser'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task
def runserver(ctx):
    """
    Run the manage.py runserver command.
    """
    cmd = 'python manage.py runserver'
    print(f"Running '{cmd}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(site_dir):
            ctx.run(cmd, pty=True)


@task(post=[check, migrate, createsuperuser, test])
def setup(ctx, debug=True):
    """
    Setup the development environment.
    """
    # create virtual environment if it doesn't exist
    if not os.path.exists(venv):
        print(f"Creating virtual environment '{venv}'.")
        ctx.run(f'python3 -m venv {venv}', pty=True)
    else:
        print(f"Not created '{venv}' because it already exists.")

    # install dependencies
    print("Installing dependencies from requirements-pinned.txt.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        ctx.run('pip install -r requirements-pinned.txt', pty=True)
        if debug:
            print("Installing dependencies from requirements-dev.txt.")
            ctx.run('pip install -r requirements-dev.txt', pty=True)

    # install selenium driver
    if debug:
        print("Getting Selenium driver.")
        driver_tar_dir = os.path.join(current_dir, "geckodriver.zip")
        driver_extract_dir = os.path.join(venv, "bin")
        driver_path = os.path.join(driver_extract_dir, "geckodriver")
        try:
            page = urllib.request.urlopen("https://github.com/mozilla/geckodriver/releases/latest/")
            page_content = str(page.read(), 'utf-8')
            latest_driver = re.search(r'<a href="/mozilla/geckodriver/releases/tag/v\d+\.\d+\.\d+">(v\d+\.\d+\.\d+)</a>', page_content).group(1)
        except:
            latest_driver = None
        if latest_driver:
            ctx.run(f'wget https://github.com/mozilla/geckodriver/releases/download/{latest_driver}/geckodriver-{latest_driver}-linux64.tar.gz -O {driver_tar_dir}', hide=True)
            ctx.run(f'tar -xf {driver_tar_dir} -C {driver_extract_dir}', hide=True)
            ctx.run(f'chmod +x {driver_path}', hide=True)
            ctx.run(f'rm {driver_tar_dir}', hide=True)

    # create local settings
    settings_template = os.path.join(settings_dir, "development.py.template")
    settings_file = os.path.join(settings_dir, "development.py")
    print(f"Creating development settings '{settings_file}' from '{settings_template}'.")
    settings_text = ""
    with open(settings_template) as f:
        settings_text = f.read()
        if not debug:
            # remove DEBUG = True
            settings_text = re.sub(r'DEBUG ?= ?True', 'DEBUG = False', settings_text)
            # remove django_extensions
            pattern_django_extensions = r"INSTALLED_APPS\.append\('django_extensions'\)"
            settings_text = re.sub(pattern_django_extensions, "", settings_text)
            # remove djdev_panel
            pattern_djdev_panel = r"MIDDLEWARE = \['djdev_panel\.middleware\.DebugMiddleware'\] \+ MIDDLEWARE"
            settings_text = re.sub(pattern_djdev_panel, "", settings_text)
            # remove RUN_SELENIUM_TESTS = True
            settings_text = re.sub(r'RUN_SELENIUM_TESTS ?= ?True', 'RUN_SELENIUM_TESTS = False', settings_text)
    with open(settings_file, 'w') as f:
        print(settings_text, file=f, end="")


namespace.add_task(setup)
namespace.add_task(check)
namespace.add_task(test)
namespace.add_task(makemigrations)
namespace.add_task(migrate)
namespace.add_task(createsuperuser)
namespace.add_task(runserver)
