# Masterpiece website

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

[Django](https://www.djangoproject.com/) powered website that allows anybody in the world to collaborate on the same artistic masterpiece at the same time. Uses Python 3 and Django 2.0.

## Motivation
I was bored. I also wanted to learn the basics of [Django Channels](https://channels.readthedocs.io/en/latest/).

### Suggested Dependencies
* postgresql
* memcached
* redis
* Firefox (for automated testing)

## Installation
1. Clone repository
2. Install [invoke](http://www.pyinvoke.org/index.html)
    * `pip3 install invoke`
3. Change directories to project root
    * `cd /path/to/cloned/project/`
4. Do initial setup
    * `invoke setup`
5. Run development server
    * `invoke runserver`

Note that these installation instructions may not work on your system. They are meant to be a general guideline and help in getting the application installed.

### Invoke Commands
Invoke is used to make life easier.

The following commands are available from the root directory:
* `setup` or `setup --no-debug`
* `check`
* `test` or `test --warnings`
* `makemigrations`
* `migrate`
* `createsuperuser`
* `runserver`

Run a command with `invoke <command>`.

Get command information with `invoke <command> -h`.

List available commands with `invoke -l`.

### License
[MIT License](LICENSE)
